#!/usr/bin/env python
# Copyright (c) 2010, xplo.re IT Services, Michael Maier.
# All rights reserved.

import os, re, sys

from datetime import datetime

try:
    import ldap
except ImportError:
    print ("Failed to import LDAP module; this tool requires python-ldap.")
    sys.exit (1)


# CONFIGURATION

# LDAP server bind DN and password.
ldap_bind_dn = None
ldap_bind_pw = None

# LDAP server connection URL and base search DN.
ldap_server = "ldap://127.0.0.1:389/"
ldap_basedn = "ou=Repositories,ou=Development,dc=acme,dc=com"

# LDAP query filter for repository roots.
ldap_query_repo = "(objectClass=repository)"
# LDAP query filter for repository paths.
ldap_query_path = "(objectClass=repositoryPath)"
# LDAP query filter for ACL entries.
ldap_query_acls = "(objectClass=pathACL)"
# LDAP attributes for real repository name and path components.
ldap_attr_repo_name = "path"
ldap_attr_path_name = "path"
# LDAP attributes for ACL DN and privileges (NOT considered mutually exclusive).
ldap_attr_acls_dn = "adn"
ldap_attr_acls_ro = "canRead"
ldap_attr_acls_rw = "canWrite"
# LDAP attribute from users and groups storing the account name.
ldap_attr_user_id = "aid"
ldap_attr_groupcn = "cn"
ldap_attr_members = "member"
# LDAP object classes to be treated as groups. Only one class needs to match.
ldap_objc_groups = ["groupOfNames", "groupOfUniqueNames"]

# Filename for generated output.
authz_output_filename = "svnaccess"

# Whether to export an aliases section (user_id = DN format).
authz_export_aliases = True

# Whether to print verbose output.
verbose = False


# GLOBALS
ldap_groups = {}
ldap_buffer = {}

Version = "1.0"


def ldap_bind():
    """Establishes connection to the LDAP server and returns the LDAP object."""

    global ldap_bind_dn
    global ldap_bind_pw
    global ldap_server
    global verbose

    ldapcon = ldap.initialize (ldap_server)

    if ldap_bind_dn:
        ldapcon.bind (ldap_bind_dn, ldap_bind_pw)

    if verbose:
        print ("LDAP connection established with %s." % ldap_server)

    return ldapcon


def ldap_read_dn (ldapcon_, dn_):
    """Reads an entry by its DN, determines groups and performs global expansion."""

    global verbose
    global ldap_buffer
    global ldap_groups
    global ldap_objc_groups

    if not (dn_ in ldap_buffer):
        result = ldapcon_.search (dn_, ldap.SCOPE_BASE)

        # Get single entry value, if any.
        type_id, data = ldapcon_.result (result, 0)

        data = data[0][1]

        # Determine, whether entry is a group.
        isGroup = False

        for objectClass in data['objectClass']:
            if objectClass in ldap_objc_groups:
                isGroup = True
                break

        # Store buffer entry.
        ldap_buffer[dn_] = [data, isGroup]

        # Determine unique identifier for new group.
        if isGroup:
            index = 1

            groupCN = groupAlias = data[ldap_attr_groupcn][0]

            while groupAlias in ldap_groups:
                groupAlias = groupCN + "-" + index
                index += 1

            ldap_groups[groupAlias] = dn_

            ldap_buffer[dn_].append (groupAlias)

    return ldap_buffer[dn_][0], ldap_buffer[dn_][1]


def ldap_parse_bool (bool_):
    """Parses LDAP Boolean string and returns Python Boolean."""

    if isinstance (bool_, list):
        bool_ = bool_[0]

    return bool_ == "TRUE"


def get_group_alias (dn_):
    """Returns group authz identifier."""

    global ldap_buffer

    return "@" + ldap_buffer[dn_][2]


def process_acl_entry (ldapcon_, acl_):
    """Formats authz ACL entry from ACL object."""

    global ldap_attr_acls_dn
    global ldap_attr_user_id

    aclDN = acl_[ldap_attr_acls_dn][0]

    token = None
    isGroup = False

    if aclDN:
        token, isGroup = ldap_read_dn (ldapcon_, aclDN)

    if "canRead" in acl_ or "canWrite" in acl_:
        privileges = ""

        if "canWrite" in acl_ and ldap_parse_bool (acl_["canWrite"]):
            privileges = "rw"
        elif "canRead" in acl_ and ldap_parse_bool (acl_["canRead"]):
            privileges = "r"

        if None == token:
            token = "*"
        elif isGroup:
            token = get_group_alias (aclDN)
        else:
            token = token[ldap_attr_user_id][0]

        return "%s = %s" % (token, privileges)


def traverse_repository_cmp (a_, b_):
    """Compares two result entries to yield lexicographical ordering with groups last."""

    a = a_.lower()
    b = b_.lower()

    if "@" == a[0] and not "@" == b[0]:
        return 1
    elif "@" == b[0] and not "@" == a[0]:
        return -1
    else:
        return cmp (a, b)


def traverse_repository (ldapcon_, rootdn_, repos_, path_):
    """Recursively scans a repository structure."""

    global ldap_query_acls

    print
    print ("[%s:%s]" % (repos_, path_))

    # Strip any slash suffix.
    path = path_.rstrip ("/")

    # Process ACL entries for current path (ordered: wildcard, users, groups).
    acls = []

    result = ldapcon_.search (rootdn_, ldap.SCOPE_ONELEVEL, ldap_query_acls)

    while True:
        type_id, data = ldapcon_.result (result, 0)

        if (ldap.RES_SEARCH_ENTRY == type_id):
            for value in data:
                acls.append (process_acl_entry (ldapcon_, value[1]))
        elif (ldap.RES_SEARCH_RESULT == type_id):
            break

    acls.sort (traverse_repository_cmp)

    for entry in acls:
        print entry

    # Process subdirectories.
    result = ldapcon_.search (rootdn_, ldap.SCOPE_ONELEVEL, ldap_query_path)

    while True:
        type_id, data = ldapcon_.result (result, 0)

        if (ldap.RES_SEARCH_ENTRY == type_id):
            for value in data:
                traverse_repository (ldapcon_, value[0], repos_, path + "/" + value[1][ldap_attr_path_name][0])
        elif (ldap.RES_SEARCH_RESULT == type_id):
            break


def output_repositories (ldapcon_, basedn_):
    """Searches and traverses managed repositories, generates authz output."""

    global ldap_query_repo
    global ldap_attr_repo_name

    repositories = []

    result = ldapcon_.search (basedn_, ldap.SCOPE_ONELEVEL, ldap_query_repo)

    while True:
        type_id, data = ldapcon_.result (result, 0)

        if (ldap.RES_SEARCH_ENTRY == type_id):
            for value in data:
                repositories.append (value)
        elif (ldap.RES_SEARCH_RESULT == type_id):
            break

    for repos in repositories:
        traverse_repository (ldapcon_, repos[0], repos[1][ldap_attr_repo_name][0], "/")


def output_aliases (ldapcon_):
    """Outputs list of user aliases (user ID as an alias of user DN)."""

    global ldap_buffer

    print
    print ("[aliases]")

    for dn in ldap_buffer:
        if ldap_buffer[dn][1]:
            # Skip groups.
            continue

        print ("%s = %s" % (ldap_buffer[dn][0][ldap_attr_user_id][0], dn))


def output_groups (ldapcon_):
    """Output accumulated group lists."""

    global ldap_groups

    print
    print ("[groups]")

    processedGroupAliases = []

    # Process a snapshot of the current list of groups until all aliases have
    # been processed (including new group expansions).
    while len (processedGroupAliases) < len (ldap_groups):
        groupExpansion = False

        for alias in ldap_groups.keys():
            if alias in processedGroupAliases:
                continue

            token, isGroup = ldap_read_dn (ldapcon_, ldap_groups[alias])

            print ("# %s" % ldap_groups[alias])
            members = []

            if ldap_attr_members in token:
                for memberDN in token[ldap_attr_members]:
                    member, isGroup = ldap_read_dn (ldapcon_, memberDN)

                    if isGroup:
                        members.append (get_group_alias (memberDN))
                    elif ldap_attr_user_id in member:
                        members.append (member[ldap_attr_user_id][0])

            print ("%s = %s" % (alias, ", ".join (members)))

            processedGroupAliases.append (alias)


def main():
    global ldap_basedn
    global ldap_server

    print "# Auto-generated Subversion ACL dump from LDAP"
    print "# Generated by xplo.re IT Services LDAP to Authz Export Script v" + Version
    print "#"
    print "# Date:   " + datetime.now().strftime ("%Y-%m-%d %H:%M:%S %Z")
    print "# Server: " + ldap_server
    print "# BaseDN: " + ldap_basedn

    ldapcon = ldap_bind()
    output_repositories (ldapcon, ldap_basedn)
    output_groups (ldapcon)

    if authz_export_aliases:
        output_aliases (ldapcon)


if __name__ == "__main__":
    main()
